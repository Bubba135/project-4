﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour {

	public static GameMaster gm;
	public static GameObject gameM; 
	public static GameMaster instance; 


	void Awake(){
		if (instance == null) {
			instance = this; 
		} else {
			Destroy (gameObject); 
		}
		
	
	
	}

	void Start () {
	}

	public Transform playerPrefab;
	public Transform spawnPoint;
	public int spawnDelay = 2;

	public IEnumerator RespawnPlayer () {
		yield return new WaitForSeconds (spawnDelay);
		
		Instantiate (playerPrefab, spawnPoint.position, spawnPoint.rotation);
	}

	public static void KillPlayer (Player player) {
		Destroy (player.gameObject);
		gm.StartCoroutine (gm.RespawnPlayer ());
	}

}


/*void Start () {
	if (gm == null) {
		gm = GameObject.FindGameObjectsWithTag ("GM").GetComponent<GameMaster>();
	}
}*/