﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTaril : MonoBehaviour {

	public int moveSPeed = 230;
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.right * Time.deltaTime * moveSPeed);
		Destroy (gameObject, 1);
	}
}
