﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallaxing : MonoBehaviour {

	public Transform[] backGround;  //Array of  all the background
	private float [] parralaxScales; // the proportion 
	public float smoothing = 1f; // who smooth the parralaxs will be.

	private Transform cam;
	private Vector3 previousCamPos;

	// Is called befor start().
	void Awake () {
		//set cam referece
		cam = Camera.main.transform;
	}

	// Use this for initialization
	void Start () {
		// The previous frame 
		previousCamPos = cam.position;

		parralaxScales = new float[backGround.Length];

		for (int i = 0; i < backGround.Length; i++) {
			parralaxScales [i] = backGround [i].position.z * -1;
		}
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < backGround.Length; i++) {
			float parallax = (previousCamPos.x - cam.position.x) * parralaxScales [i];

			float backgroundTargetPosX = backGround [i].position.x + parallax;

			Vector3 backgroundTargetPos = new Vector3 (backgroundTargetPosX, backGround [i].position.y, backGround [i].position.z);

			backGround [i].position = Vector3.Lerp (backGround [i].position, backgroundTargetPos, smoothing * Time.deltaTime);
		}
		previousCamPos = cam.position;
	}
}
